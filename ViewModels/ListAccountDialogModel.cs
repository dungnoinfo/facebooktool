﻿using FacebookTool.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace FacebookTool.ViewModels
{
    class ListAccountDialogModel
    {
        public ObservableCollection<Account> Accounts { get; set; }

        public ListAccountDialogModel()
        {
            Accounts = new ObservableCollection<Account>();
            Accounts.Add(new Account() { UserName = "Hello" });
        }
    }
}
