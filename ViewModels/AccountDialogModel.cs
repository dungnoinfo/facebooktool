﻿using FacebookTool.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace FacebookTool.ViewModels
{
    class AccountDialogModel
    {
        public Account Account { get; set; }

        public AccountDialogModel()
        {
            Account = new Account() { UserName = "Hello 2", LoginType = Account.LoginTypes.COOKIE, Status =  Account.StatusTypes.CHECKPOINT };
        }
    }
}
