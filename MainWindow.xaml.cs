﻿using FacebookTool.Repositories;
using FacebookTool.ViewModels;
using FacebookTool.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FacebookTool
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Grid_Initialized(object sender, EventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            AccountRepo.instance.Create(new Models.Account() { UserName = "Hello" });
        }

        private void ShowListAccount(object sender, RoutedEventArgs e)
        {
            var dialog = new ListAccountDialog();
            dialog.DataContext = new ListAccountDialogModel();
            if (dialog.ShowDialog() == true)
            {
                MessageBox.Show("You said: ");
            }
        }
    }
}
