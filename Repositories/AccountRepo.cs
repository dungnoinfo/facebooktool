﻿using FacebookTool.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FacebookTool.Repositories
{
    class AccountRepo: BaseRepo<Account>
    {
        public static AccountRepo instance = new AccountRepo();
    }
}
