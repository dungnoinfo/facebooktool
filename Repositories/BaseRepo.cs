﻿using FacebookTool.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacebookTool.Repositories
{
    class BaseRepo<TEntity> where TEntity : BaseModel
    {
        private DatabaseContext context = DatabaseContext.database;

        public int Create(TEntity entity)
        {
            context.Set<TEntity>().Add(entity);
            return context.SaveChanges();
        }

        public void Update(TEntity entity)
        {
            context.Set<TEntity>().Update(entity);
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            var entity = GetById(id);
            context.Set<TEntity>().Remove(entity);
            context.SaveChanges();
        }

        public TEntity GetById(int id)
        {
            return context.Set<TEntity>()
                        .FirstOrDefault(e => e.Id == id);
        }
    }
}
