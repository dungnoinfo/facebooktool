﻿using FacebookTool.Actions;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacebookTool.Services
{
    class FacebookService
    {
        private SeleniumService seleniumService;

        public FacebookService()
        {
            seleniumService = new SeleniumService();
        }

        public void Start()
        {
            var chromeOptions = new ChromeOptions();
            string userAgent = UltilService.RandomUA();
            seleniumService.CreateDriver(chromeOptions, userAgent);
            FacebookLoginAction loginAction = new FacebookLoginAction();
            seleniumService.DispatchAction(loginAction);
        }
    }
}
