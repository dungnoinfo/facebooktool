﻿using FacebookTool.Actions;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FacebookTool.Services
{
    class SeleniumService
    {
        private ChromeDriver driver;

        public SeleniumService()
        {

        }

        public void CreateDriver(ChromeOptions options, string userAgent)
        {
            //options.AddArgument("--no-sandbox");
            //options.AddArgument("--disable-dev-shm-usage");
            //options.AddArgument("--enabled");
            //options.AddArgument("--user-agent=\"" + userAgent + "\"");
            driver = new ChromeDriver(@"Assets", options);
            //options.BinaryLocation = @"C:\Users\Tien Dung\Downloads\GoogleChromePortable\GoogleChromePortable.exe";
            //driver = new ChromeDriver(@"C:\Users\Tien Dung\Downloads\chromedriver_win32 (2)", options);
            Console.WriteLine("đá");
        }

        public bool TryToQuit()
        {
            try
            {
                driver.Quit();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        private void customExtension()
        {
            //var url = "edge://version/";
            //driver.Url = url;
            //driver.Navigate().GoToUrl(url);
            //Thread.Sleep(2000);
            //IWebElement element = driver.FindElement(By.XPath("//td[@id=\"command_line\"]"));

            //var extensionPath = Regex.Match(element.Text, "--load-extension=\"(.*)internal\"").Value.Replace("--load-extension=", "").Replace("\"", "");
            //System.IO.File.WriteAllText(extensionPath + "/manifest.json", System.IO.File.ReadAllText(@"extensions\manifest.json"));
            //System.IO.File.WriteAllText(extensionPath + "/background.js", System.IO.File.ReadAllText(@"extensions\background.js"));

            //driver.Url = "edge://extensions/";
            //driver.Navigate().GoToUrl("edge://extensions/");
            //Thread.Sleep(2000);
            //this.clickElement(By.XPath("//*[contains(text(),'Reload')]"));
        }

        public void Stop()
        {
            try
            {
                driver.Quit();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public void Focus()
        {
            try
            {
                Thread.Sleep(2000);
                driver.ExecuteScript("window.focus();");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public bool WaitAndCheckExistsElement(By by, int wait = 5)
        {
            for (var i = 0; i < wait; i++)
            {
                if (this.CheckExistsElement(by))
                {
                    return true;
                }
                Thread.Sleep(1000);
            }

            return false;
        }

        public IWebElement WaitAndGetElement(By by, int wait = 5)
        {
            for (var i = 0; i < wait; i++)
            {
                var element = GetElement(by);
                if (element != null)
                {
                    return element;
                }
                Thread.Sleep(1000);
            }

            return null;
        }

        public IWebElement GetElement(By by)
        {
            try
            {
                IWebElement element = driver.FindElement(by);
                return element;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool CheckExistsElement(By by)
        {
            try
            {
                IWebElement element = driver.FindElement(by);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void InputElement(By by, string value)
        {
            IWebElement textBox = WaitAndGetElement(by); ;
            textBox.Clear();
            Thread.Sleep(500);
            var keys = value.ToCharArray();
            foreach (var key in keys)
            {
                textBox.SendKeys(key.ToString());
                UltilService.RandomSleep(100, 300);
            }
        }

        public void ClickElement(By by, int wait = 5)
        {
            IWebElement element = WaitAndGetElement(by, wait);
            element.Click();
        }

        public bool DispatchAction(BaseAction action)
        {
            action.seleniumService = this;
            return action.Start();
        }

        public void GotoUrl(string url)
        {
            driver.Navigate().GoToUrl(url);
        }
    }
}
