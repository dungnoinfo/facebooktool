﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FacebookTool.Models
{
    class ComboboxItem
    {
        public string Display { get; set; }
        public object Value { get; set; }

        public ComboboxItem(string Display, object Value)
        {
            this.Display = Display;
            this.Value = Value;
        }
    }
}
