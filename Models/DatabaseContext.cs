﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace FacebookTool.Models
{
    class DatabaseContext : DbContext
    {
        public static DatabaseContext database = new DatabaseContext();

        public DbSet<Activity> Activities { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Requirement> Requirements { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(@"Data Source=C:\Users\Tien Dung\Documents\database.db");
        }
    }
}
