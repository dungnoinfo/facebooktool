﻿namespace FacebookTool.Models
{
    class Account : BaseModel
    {
        public enum LoginTypes
        {
            USERS,
            COOKIE,
            PROFILE
        }

        public enum StatusTypes
        {
            OK,
            ERROR,
            CHECKPOINT
        }

        public string UserName { get; set; }
        public string Password { get; set; }
        public LoginTypes LoginType { get; set; }
        public string Cookie { get; set; }
        public string Profile { get; set; }
        public StatusTypes Status { get; set; }
        public string Error { get; set; }
    }
}
