﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FacebookTool.Models
{
    class Requirement : BaseModel
    {
        public enum DestinationTypes
        {
            USERS,
            COOKIE,
            PROFILE
        }

        public enum StatusTypes
        {
            OK,
            CANCEL,
            PROCESSING
        }

        public string DestinationId { get; set; }
        public DestinationTypes DestinationType { get; set; }
        public int Value { get; set; }
        public StatusTypes Status { get; set; }
    }
}
