﻿using System;
using System.Collections.Generic;
using System.Text;
using static FacebookTool.Models.Requirement;

namespace FacebookTool.Models
{
    class Activity : BaseModel
    {
        public enum StatusTypes
        {
            OK,
            ERROR,
            PROCESSING
        }

        public string DestinationId { get; set; }
        public DestinationTypes DestinationType { get; set; }
        public int AccountId { get; set; }
        public int RequirementId { get; set; }
        public StatusTypes Status { get; set; }
        public string Error { get; set; }
    }
}
