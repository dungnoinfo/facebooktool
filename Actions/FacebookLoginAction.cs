﻿using FacebookTool.Services;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacebookTool.Actions
{
    class FacebookLoginAction : BaseAction
    {
        public override bool Start()
        {
            GoToFacebook();
            UltilService.RandomSleep(200, 300);
            InputUserInfo("0898367332", "280599A");
            ClickSignin();
            return true;
        }

        public void GoToFacebook()
        {
            seleniumService.GotoUrl("https://facebook.com");
        }

        public void InputUserInfo(string username, string password)
        {
            seleniumService.InputElement(By.Id("email"), username);
            seleniumService.InputElement(By.Id("pass"), password);
            UltilService.RandomSleep(500, 1000);
        }

        public void ClickSignin()
        {
            seleniumService.ClickElement(By.XPath("//input[@value=\"Đăng nhập\"]"));
        }
    }
}
