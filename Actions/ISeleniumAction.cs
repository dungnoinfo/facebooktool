﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacebookTool.Actions
{
    interface ISeleniumAction
    {
        bool Start();
    }
}
