﻿using FacebookTool.Services;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacebookTool.Actions
{
    abstract class BaseAction : ISeleniumAction
    {
        public SeleniumService seleniumService;

        public abstract bool Start();
    }
}
