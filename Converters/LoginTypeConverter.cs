﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Data;

namespace FacebookTool.Converters
{
    class LoginTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var index = int.Parse(parameter.ToString());
            return index == (int)value ? "Hidden" : "Visible";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var index = int.Parse(parameter.ToString());
            return index == (int)value ? "Hidden" : "Visible";
        }
    }
}
