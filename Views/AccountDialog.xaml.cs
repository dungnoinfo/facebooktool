﻿using FacebookTool.Models;
using FacebookTool.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FacebookTool.Views
{
    /// <summary>
    /// Interaction logic for AccountDialog.xaml
    /// </summary>
    public partial class AccountDialog : Window
    {
        public AccountDialog()
        {
            InitializeComponent();
            AddLoginTypeBox();
            AddStatusBox();
        }

        private void AddLoginTypeBox()
        {
            List<ComboboxItem> comboboxItems = new List<ComboboxItem>();
            comboboxItems.Add(new ComboboxItem("Password", Account.LoginTypes.USERS));
            comboboxItems.Add(new ComboboxItem("Profile", Account.LoginTypes.PROFILE));
            comboboxItems.Add(new ComboboxItem("Cookie", Account.LoginTypes.COOKIE));
            cmbLoginType.ItemsSource = comboboxItems;
        }

        private void AddStatusBox()
        {
            List<ComboboxItem> comboboxItems = new List<ComboboxItem>();
            comboboxItems.Add(new ComboboxItem("OK", Account.StatusTypes.OK));
            comboboxItems.Add(new ComboboxItem("Error", Account.StatusTypes.ERROR));
            comboboxItems.Add(new ComboboxItem("Check point", Account.StatusTypes.CHECKPOINT));
            cmbStatusType.ItemsSource = comboboxItems;
        }

        private void Window_Initialized(object sender, EventArgs e)
        {
            
        }
    }
}
