﻿using FacebookTool.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FacebookTool.Views
{
    /// <summary>
    /// Interaction logic for ListAccountDialog.xaml
    /// </summary>
    public partial class ListAccountDialog : Window
    {
        public ListAccountDialog()
        {
            InitializeComponent();
        }

        private void btnAddNewAccount_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new AccountDialog();
            dialog.DataContext = new AccountDialogModel();
            dialog.Show();
        }
    }
}
